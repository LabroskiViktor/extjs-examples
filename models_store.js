//Contact Model extends data model
Ext.define('ContactModel', {
  extend: 'Ext.data.Model',
  fields: [{ name: '_id', type: 'string' }, { name: 'name', type: 'string' }],

  //   validations: [
  //     { type: 'length', field: 'title', min: 3 },
  //     { type: 'inclusion', field: 'sessionLevel', list: [1, 2, 3] }
  //   ],

  proxy: {
    type: 'rest',
    url: 'http://localhost:8080/api/bears',
    // noCache: false,
    reader: {
      type: 'json'
    }
  }
});

// var sessionBringBack = Ext.ModelManager.getModel('ContactModel');
// sessionBringBack.load('5b6368d7a9857c2a9224c1f8', {
//   success: function(session) {
//     console.log(session.getData());
//     // session.destroy();
//   }
// });

// var contact = Ext.create('ContactModel', {
//   name: 'Marija',
// });

// Create Record
// contact.save({
//   success: function(session) {
//     console.log(session.getData());
//   },
//   faliure : function(error){
//     console.log(error);
//   }
// });

// ===========================================
// Ext.Ajax.request({
//   url: 'http://localhost:8080/api/bears/' + '5b6363a31c3f752125495123',
//   method: 'DELETE',
//   success: function(success) {
//     Ext.Msg.alert('Success');
//     console.log(success);
//   },
//   failure: function() {
//     Ext.Msg.alert('Fail');
//   }
//   // jsonData: JSON.stringify(contact.raw)
// });

//Validations
// if (!mySession.isValid()) {
//   var errors = mySession.validate();
//   errors.each(function(rec) {
//     console.log(rec);
//   });
// } else {
//   console.log(mySession.raw);
// }
// =============================================
