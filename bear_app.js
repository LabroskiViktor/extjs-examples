var editBearForm = Ext.create('Ext.window.Window', {
	padding: 5,
	width: 400,
	title: 'Edit Bears',
	model: 'true',
	closeAction: 'hide',
	items: [
		{
			xtype: 'form',
			bodyPadding: 10,
			title: '',
			defaults: {
				labelWidth: 90,
				margin: '0 0 10 0',
				anchor: '90%'
			},
			items: [
				{
					xtype: 'textfield',
					name: 'name',
					fieldLabel: 'Name'
				},
				{
					xtype: 'textfield',
					name: '_id',
					fieldLabel: 'Id'
				}
			]
		},
		{
			xtype: 'container',
			padding: '5 5 5 5',
			layout: {
				type: 'hbox',
				align: 'middle',
				pack: 'center'
			},
			items: [
				{
					xtype: 'button',
					text: 'Update',
					handler: function(button) {
						var formData = button
							.up()
							.up()
							.down('form')
							.getValues();
						Ext.Ajax.request({
							url:
								'http://localhost:8080/api/bears/' +
								formData._id,
							method: 'PUT',
							success: function(success) {
								button.up('window').hide();
								bearGridPanel.store.load();
								console.log(success);
							},
							failure: function(error) {
								console.log(error);
							},
							jsonData: JSON.stringify(formData)
						});
					}
				},
				{
					xtype: 'button',
					text: 'Cancel',
					handler: function(button) {
						button.up('window').hide();
					}
				},
				{
					xtype: 'button',
					text: 'Delete',
					handler: function(button) {
						var formData = button
							.up()
							.up()
							.down('form')
							.getValues();
						Ext.Ajax.request({
							url:
								'http://localhost:8080/api/bears/' +
								formData._id,
							method: 'DELETE',
							success: function(success) {
								button.up('window').hide();
								bearGridPanel.store.load();
								console.log(success);
							},
							failure: function(error) {
								console.log(error);
							}
						});
					}
				}
			]
		}
	]
});

var bearGridPanel = Ext.create('Ext.grid.Panel', {
	listeners: {
		itemdblclick: function(gridpanel, record, item, e) {
			var form = editBearForm.down('form');
			form.loadRecord(record);
			editBearForm.show();
		}
	},
	store: {
		fields: ['_id', 'name'],
		autoLoad: true,
		autoSync: true,
		proxy: {
			type: 'rest',
			url: 'http://localhost:8080/api/bears',
			readers: {
				type: 'json'
			}
		},
		sorters: [{ property: 'name' }]
	},
	columns: [
		{
			xtype: 'gridcolumn',
			dataIndex: '_id',
			text: 'Id'
		},
		{
			xtype: 'gridcolumn',
			dataIndex: 'name',
			text: 'Name',
			flex: 1,
			minWidth: 100,
			width: 75
		}
	]
});

var addBearForm = Ext.create('Ext.window.Window', {
	padding: 5,
	width: 400,
	title: 'Add Bear',
	model: 'true',
	closeAction: 'hide',
	items: [
		{
			xtype: 'form',
			bodyPadding: 10,
			title: '',
			defaults: {
				labelWidth: 90,
				margin: '0 0 10 0',
				anchor: '90%'
			},
			items: [
				{
					xtype: 'textfield',
					name: 'name',
					fieldLabel: 'Name'
				}
			]
		},
		{
			xtype: 'container',
			padding: '5 5 5 5',
			layout: {
				type: 'hbox',
				align: 'middle',
				pack: 'center'
			},
			items: [
				{
					xtype: 'button',
					text: 'Add',
					handler: function(button) {
						var formData = button
							.up()
							.up()
							.down('form')
							.getValues();
						Ext.Ajax.request({
							url: 'http://localhost:8080/api/bears/',
							method: 'POST',
							success: function(success) {
								button.up('window').hide();
								bearGridPanel.store.load();
								button
									.up()
									.up()
									.down('form')
									.getForm()
									.reset();
								console.log(success);
							},
							failure: function(error) {
								console.log(error);
							},
							jsonData: JSON.stringify(formData)
						});
					}
				},
				{
					xtype: 'button',
					text: 'Cancel',
					handler: function(button) {
						button.up('window').hide();
					}
				}
			]
		}
	]
});

var bearsPanel = Ext.create('Ext.Panel', {
	title: 'Bears Panel',
	region: 'center',
	flex: 1,
	padding: 10,
	items: [
		bearGridPanel,
		{
			xtype: 'button',
			text: 'Add Bear',
			margin: '5 5 5 5',
			handler: function() {
				addBearForm.show();
			}
		}
	]
});

// var speaksersPanel = Ext.create('Ext.Panel', {
// 	title: 'Speaksers Panel',
// 	region: 'center',
// 	flex: 3
// });

// var detailsPanel = Ext.create('Ext.Panel', {
// 	title: 'Details Panel',
// 	region: 'east',
// 	flex: 1
// });

Ext.application({
	name: 'MyApp',
	launch: function() {
		Ext.create('Ext.container.Viewport', {
			layout: 'border',
			items: [bearsPanel]
		});
	}
});
