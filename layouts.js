var comp1 = Ext.create('Ext.Component', {
    title: 'Width = 25%',
    columnWidth: 0.25,
    html: 'Content'
  });
  
  var comp2 = Ext.create('Ext.Component', {
    title: 'Width = 75%',
    columnWidth: 0.75,
    html: 'Content'
  });
  
  var comp3 = Ext.create('Ext.Component', {
    title: 'Width = 250px',
    width: 250,
    html: 'Content'
  });
  
  var myContainer = Ext.create('Ext.container.Container', {
    layout: 'column',
    padding: 20,
    items: [
      {
        title: 'Width = 25%',
        columnWidth: 0.25,
        html: 'Content'
      },
      {
        title: 'Width = 75%',
        columnWidth: 0.75,
        html: 'Content'
      },
      {
        title: 'Width = 250px',
        width: 250,
        html: 'Content'
      }
    ]
  });
  
  Ext.application({
    name: 'MyApp',
    launch: function() {
      Ext.create('Ext.container.Viewport', {
        items: [myContainer]
      });
    }
  });
  