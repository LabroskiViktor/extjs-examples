//Util example
var updateConsole = function() {
  console.log(Ext.Date.format(new Date(), 'g:i:s A'));
};

var runner = new Ext.util.TaskRunner();
task = runner.start({
  run: updateConsole,
  interval: 3000
});

//Class example
Ext.define('MyApp.Session', {
  config: {
    title: '',
    description: '',
    level: 'begginer'
  },
  applyTitle: function(title) {
    if (title === undefined) {
      alert('title undefined!');
    }
    return title;
  },
  constructor: function(config) {
    this.initConfig(config);
  }
});

var session = Ext.create('MyApp.Session', {});

session.setTitle('Viktors session title');

console.log(session.getTitle());
